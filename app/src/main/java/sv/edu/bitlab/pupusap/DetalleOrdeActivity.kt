package sv.edu.bitlab.pupusap

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_detalle_orden.*
import sv.edu.bitlab.pupusap.HistoryScreen.HistoryRecyclerView.DetalleAdapter
import sv.edu.bitlab.pupusap.HistoryScreen.HistoryRecyclerView.DetalleViewHolder
import sv.edu.bitlab.pupusap.Models.Orden
import java.text.DecimalFormat

class DetalleOrdeActivity : AppCompatActivity(), DetalleViewHolder.RowListener {


  override fun displayDetalle(
    detailTexview: TextView,
    priceTextView: TextView,
    contador: Int,
    index: Int
  ) {
    var total = 0.0f
    if(contador > 0){
      val totalUnidad = contador * VALOR_PUPUSA
      val descripcion = getDescripcion(index)
      detailTexview.text = getString(R.string.pupusa_line_item_description,
        contador, descripcion)
      total += totalUnidad
      val precio = DecimalFormat("$#0.00").format(totalUnidad)
      priceTextView.text = precio
    } else{
      detailTexview.visibility = View.GONE
      priceTextView.visibility = View.GONE
      /*idconstraint.removeView(detailTexview)
      idconstraint.removeView(priceTextView)*/
    }
  }

  var orden = Orden()
  var arroz = arrayListOf<Int>()
  var maiz = arrayListOf<Int>()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_detalle_orden)

    var totalid: TextView= findViewById(R.id.lineItemPriceTotal)
    val params = this.intent.extras
    arroz = params!!.getIntegerArrayList(CONTADOR_ARROZ)!!
    maiz = params.getIntegerArrayList(CONTADOR_MAIZ)!!
    val arr = arroz + maiz

    recyclerDetalle.layoutManager = LinearLayoutManager(this)
    recyclerDetalle.adapter = DetalleAdapter(arr as ArrayList<Int>,this)

    var total: Float=0.0f

    for (item in arr){
        total += (item*0.5f)
    }
    val df = DecimalFormat("#.##")
    val totalorden = df.format(total)
    if (total>0) totalid.text = getString(R.string.pupusa_line_item_price,totalorden)

  }


  fun getDescripcion(index: Int): String {
    return when(index){
      QUESO -> "Queso de arroz"
      FRIJOLES -> "Frijol con queso de arroz"
      REVUELTAS -> "Revueltas de arroz"
      JALAPEÑO -> "Jalapeño de arroz"
      AJO -> "Ajo de arroz"
      QUESO_MAIZ-> "Queso de maiz"
      FRIJOLES_MAIZ -> "Frijol con queso de maiz"
      REVUELTAS_MAIZ -> "Revueltas de maiz"
      JALAPEÑO_MAIZ -> "Jalapeño de maiz"
      AJO_MAIZ -> "Ajo de maiz"
      else -> throw RuntimeException("Pupusa no soportada")
    }
  }

  companion object{
    const val QUESO = 0//3
    const val FRIJOLES = 1//4
    const val REVUELTAS = 2//5
    const val JALAPEÑO = 3
    const val AJO = 4
    const val QUESO_MAIZ = 5//3
    const val FRIJOLES_MAIZ = 6//4
    const val REVUELTAS_MAIZ = 7//5
    const val JALAPEÑO_MAIZ = 8
    const val AJO_MAIZ = 9
    const val CONTADOR_ARROZ = "ARROZ"
    const val CONTADOR_MAIZ = "MAIZ"
    const val VALOR_PUPUSA = 0.5F
    const val FRAGMENT_TAG = "LIST_FRAGMENT_TAG"
  }
}
