package sv.edu.bitlab.pupusap.HistoryScreen.HistoryRecyclerView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.content.contentValuesOf
import androidx.recyclerview.widget.RecyclerView
import sv.edu.bitlab.pupusap.R

class ButtonsAdapter(var maiz: ArrayList<String>,var arroz: ArrayList<String>, val listener: ButtonsViewHolder.RellenoListener) : RecyclerView.Adapter<ButtonsViewHolder>() {

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ButtonsViewHolder {
    val view: View = LayoutInflater.from(parent.context).inflate(R.layout.button_row, parent, false)
    return ButtonsViewHolder(view, listener)
  }

  override fun getItemCount(): Int {
   return maiz.size
  }

  override fun onBindViewHolder(holder: ButtonsViewHolder, position: Int) {

    holder.onBindData(maiz[position],arroz[position], position)
  }
}