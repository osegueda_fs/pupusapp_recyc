package sv.edu.bitlab.pupusap

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import sv.edu.bitlab.pupusap.DetalleOrdeActivity.Companion.CONTADOR_ARROZ
import sv.edu.bitlab.pupusap.DetalleOrdeActivity.Companion.CONTADOR_MAIZ
import sv.edu.bitlab.pupusap.HistoryScreen.HistoryRecyclerView.ButtonsAdapter
import sv.edu.bitlab.pupusap.HistoryScreen.HistoryRecyclerView.ButtonsViewHolder
import sv.edu.bitlab.pupusap.Models.Orden

import java.text.FieldPosition

class MainActivity : AppCompatActivity(), ButtonsViewHolder.RellenoListener {
    var botonMaiz = hashMapOf<String, Button>()
    var botonArroz = hashMapOf<String, Button>()

    val orden = Orden()

    override fun onClickMaiz(relleno: String, boton1: HashMap<String, Button>) {
        orden.maiz[relleno] = orden.maiz[relleno]!! + 1
        val contador = orden.maiz[relleno]
        val resource = pupusaStringResources[relleno]
        val text = this.resources.getString(resource!!, contador)
        boton1[relleno]!!.text = text
    }

    override fun onClickArroz(relleno: String, boton2: HashMap<String, Button>) {
        orden.arroz[relleno] = orden.arroz[relleno]!! + 1
        val contador =  orden.arroz[relleno]
        val resource = pupusaStringResources[relleno]
        val text = this.resources.getString(resource!!, contador)
        boton2[relleno]!!.text = text
    }

    override fun onClickCounter(boton: HashMap<String, Button>) {
        when(boton){
            botonMaiz->{
                for ((key,value) in orden.maiz){
                    val resource = pupusaStringResources[key]
                    val text = this.resources.getString(resource!!, value)
                    boton[key]!!.text = text
                }
            }
            botonArroz->{
                for ((key,value) in orden.arroz){
                    val resource = pupusaStringResources[key]
                    val text = this.resources.getString(resource!!, value)
                    boton[key]!!.text = text
                }
            }
        }

    }

       //val orden = Orden()

    val pupusaStringResources = hashMapOf(
        Queso to R.string.pupusa_queso,
        Frijoles to R.string.frijol_con_queso,
        Revueltas to R.string.revueltas,
        Jalapeño to R.string.jalapeno,
        Ajo to R.string.ajo
    )

    /*var botonesMaiz = hashMapOf<String, Button>()
    var botonesArroz = hashMapOf<String, Button>()
    var quesoIzquierda: Button? = null
    var frijolIzquierda: Button? = null
    var revueltaIzquierda: Button? = null

    var quesoDerecha: Button? = null
    var frijolDerecha: Button? = null
    var revueltasDerecha: Button? = null
    var loadingContainer: View? = null*/

    var sendButton: Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var maiz: ArrayList<String> = ArrayList()
        var arroz: ArrayList<String> = ArrayList()

        maiz.add("Frijoles")
        maiz.add("Queso")
        maiz.add("Revueltas")
        maiz.add("Jalapeño")
        maiz.add("Ajo")

        arroz.add("Frijoles")
        arroz.add("Queso")
        arroz.add("Revueltas")
        arroz.add("Jalapeño")
        arroz.add("Ajo")


        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = ButtonsAdapter(maiz,arroz, this)

        sendButton = findViewById(R.id.sendButton)
        sendButton!!.setOnClickListener {
            confirmarOrden()
        }

      /*quesoIzquierda = findViewById(R.id.quesoIzquierda)
        frijolIzquierda = findViewById(R.id.frijolIzquierdaMaiz)
        revueltaIzquierda = findViewById(R.id.revueltasIzquierda)*/

        /*botonesMaiz= hashMapOf(
            QUESO to quesoIzquierda!!,
            FRIJOLES to frijolIzquierda!!,
            REVUELTAS to revueltaIzquierda!!
        )*/

        /*quesoIzquierda!!.setOnClickListener { addMaiz(QUESO) }
        frijolIzquierda!!.setOnClickListener { addMaiz(FRIJOLES) }
        revueltaIzquierda!!.setOnClickListener { addMaiz(REVUELTAS) }


        quesoDerecha = findViewById(R.id.quesoDerecha)
        frijolDerecha = findViewById(R.id.frijolIDerechaArroz)
        revueltasDerecha = findViewById(R.id.revueltasDerecha)

        botonesArroz= hashMapOf(
            QUESO to quesoDerecha!!,
            FRIJOLES to frijolDerecha!!,
            REVUELTAS to revueltasDerecha!!
        )

        quesoDerecha!!.setOnClickListener { addArroz(QUESO) }
        frijolDerecha!!.setOnClickListener { addArroz(FRIJOLES) }
        revueltasDerecha!!.setOnClickListener { addArroz(REVUELTAS) }

        sendButton = findViewById(R.id.sendButton)
        sendButton!!.setOnClickListener {
            confirmarOrden()
        }

        loadingContainer = findViewById(R.id.loadingContainer)
        loadingContainer!!.setOnClickListener { showLoading(false) }

        displayCounters()
        setActionBar(null)
        Log.d("ACTIVITY", "MainActivity onCreate()")*/
    } //endOncreate

   /* fun displayCounters(boton: HashMap<String, Button>) {
        for ((key,value) in maiz){
            val resource = pupusaStringResources[key]
            val text = this.resources.getString(resource!!, value)
            boton[key]!!.text = text
        }


        for ((key,value) in arroz){
            val resource = pupusaStringResources[key]
            val text = this.resources.getString(resource!!, value)
            boton[key]!!.text = text
        }

    }*/

   /* fun addMaiz(relleno: String) {
        maiz[relleno] = maiz[relleno]!! + 1
        val contador = maiz[relleno]
        val resource = pupusaStringResources[relleno]
        val text = this.resources.getString(resource!!, contador)
        boton[relleno]!!.text = text
    }
    fun addArroz(relleno: String) {
        arroz[relleno] = arroz[relleno]!! + 1
        val contador =  arroz[relleno]
        val resource = pupusaStringResources[relleno]
        val text = this.resources.getString(resource!!, contador)
        boton[relleno]!!.text = text
    }*/

    private fun confirmarOrden() {
        val intent = Intent(this, DetalleOrdeActivity::class.java)
        val maizz = arrayListOf(
            orden.maiz[Queso]!!,
            orden.maiz[Frijoles]!!,
            orden.maiz[Revueltas]!!,
            orden.maiz[Jalapeño]!!,
            orden.maiz[Ajo]!!)
        val arrozz = arrayListOf(
            orden.arroz[Queso]!!,
            orden.arroz[Frijoles]!!,
            orden.arroz[Revueltas]!!,
            orden.arroz[Jalapeño]!!,
            orden.arroz[Ajo]!!)

        intent.putExtra(CONTADOR_MAIZ,maizz)
        intent.putExtra(CONTADOR_ARROZ,arrozz)

        this.startActivity(intent)
    }

    /*fun showLoading(show: Boolean) {
        val visibility = if(show) View.VISIBLE else View.GONE
        loadingContainer!!.visibility = visibility
    }


    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)

    }

    companion object{
        const val MAIZ = "MAIZ"
        const val ARROZ = "ARROZ"
    }*/

}
