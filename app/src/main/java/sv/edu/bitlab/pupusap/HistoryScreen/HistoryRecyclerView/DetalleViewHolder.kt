package sv.edu.bitlab.pupusap.HistoryScreen.HistoryRecyclerView

import android.view.View
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import sv.edu.bitlab.pupusap.R


class DetalleViewHolder(itemView: View, val listener: RowListener):RecyclerView.ViewHolder(itemView) {

  fun onBindData(contador: Int, index: Int){
    var detailTexview: TextView = itemView.findViewById(R.id.lineItemDetail)
    var priceTextView: TextView = itemView.findViewById(R.id.lineItemPrice)
    var idconstraint: ConstraintLayout = itemView.findViewById(R.id.constraintid)
      listener.displayDetalle(detailTexview, priceTextView, contador, index)

  }


  interface RowListener{
    fun displayDetalle(
      detailTexview: TextView,
      priceTextView: TextView,
      contador: Int,
      index: Int
    )
  }


}