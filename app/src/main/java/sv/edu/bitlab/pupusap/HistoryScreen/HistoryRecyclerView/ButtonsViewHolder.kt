package sv.edu.bitlab.pupusap.HistoryScreen.HistoryRecyclerView

import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.RecyclerView
import sv.edu.bitlab.pupusap.R

class ButtonsViewHolder(itemView: View, val listener: RellenoListener):RecyclerView.ViewHolder(itemView) {
  //lateinit var button1: Button
  //lateinit var button2: Button
  var botonMaiz = hashMapOf<String, Button>()
   var botonArroz = hashMapOf<String, Button>()

  fun onBindData(maiz: String, arroz: String, position: Int){
    var button1: Button = itemView.findViewById(R.id.botonIzquierdaMaiz)
    var button2:Button = itemView.findViewById(R.id.botonDerechaArroz)
    button1.text=maiz
    button2.text=arroz

    botonMaiz = hashMapOf(
      maiz to button1!!
    )
    botonArroz = hashMapOf(
      arroz to button2!!
    )
    //button1.setOnClickListener{listener.onClickMaiz(maiz, position)}

    button1.setOnClickListener{
      listener.onClickCounter(botonMaiz)
      listener.onClickMaiz(maiz, botonMaiz)
    }

    button2.setOnClickListener{
      listener.onClickCounter(botonArroz)
      listener.onClickArroz(arroz, botonArroz)
    }


  }
  interface RellenoListener{
    fun onClickCounter(boton: HashMap<String, Button>)
    fun onClickMaiz(relleno: String, boton1: HashMap<String, Button>)
    fun onClickArroz(relleno: String, boton2: HashMap<String, Button>)

  }
}