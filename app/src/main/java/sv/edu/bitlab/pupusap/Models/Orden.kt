package sv.edu.bitlab.pupusap.Models

import android.os.Parcel
import android.os.Parcelable
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class Orden() : Parcelable {
  var precioUnidad = 0.5f
  var textInput = ""

  var maiz = hashMapOf(
    Frijoles to 0,
    Queso to 0,
    Revueltas to 0,
    Jalapeño to 0,
    Ajo to 0
  )
  var arroz = hashMapOf(
    Frijoles to 0,
    Queso to 0,
    Revueltas to 0,
    Jalapeño to 0,
    Ajo to 0
  )
  private var fecha:DateTime = DateTime()

  override fun writeToParcel(dest: Parcel?, flags: Int) {
    dest!!.writeFloat(this.precioUnidad)
    dest.writeInt(maiz[Queso]!!)
    dest.writeInt(maiz[Frijoles]!!)
    dest.writeInt(maiz[Revueltas]!!)
    dest.writeInt(arroz[Queso]!!)
    dest.writeInt(arroz[Frijoles]!!)
    dest.writeInt(arroz[Revueltas]!!)
    dest.writeString(this.getFecha())
  }

  constructor(parcel: Parcel) : this() {
    precioUnidad = parcel.readFloat()
    maiz[Queso] = parcel.readInt()
    maiz[Frijoles] = parcel.readInt()
    maiz[Revueltas] = parcel.readInt()
    arroz[Queso] = parcel.readInt()
    arroz[Frijoles] = parcel.readInt()
    arroz[Revueltas] = parcel.readInt()
    setFecha(parcel.readString()!!)
  }

  override fun describeContents(): Int {
    return 0
  }

  fun getTotal(): Float {
    val totalMaiz = maiz.map { entry ->
      entry.value
    }.reduce { total, counter -> total+counter }

    val totalArroz = arroz.map { entry ->
      entry.value
    }.reduce { total, counter -> total+counter }

    return (totalArroz * precioUnidad) + (totalMaiz * precioUnidad)
  }

  fun getFecha(): String {
    val formatter = DateTimeFormat.forPattern(FORMATO_FECHA)
    return formatter.print(fecha)
  }

  fun setFecha(fecha: String) {
    val formatter = DateTimeFormat.forPattern(FORMATO_FECHA)
    this.fecha = formatter.parseDateTime(fecha)
  }


  companion object CREATOR : Parcelable.Creator<Orden> {

    const val Queso = "Queso"
    const val Frijoles = "Frijoles"
    const val Revueltas = "Revueltas"
    const val Jalapeño = "Jalapeño"
    const val Ajo = "Ajo"
    const val MAIZ = "MAIZ"
    const val ARROZ = "ARROZ"
    const val FORMATO_FECHA = "dd-MM-yyyy" //09-09-2019

    override fun createFromParcel(parcel: Parcel): Orden {
      return Orden(parcel)
    }

    override fun newArray(size: Int): Array<Orden?> {
      return arrayOfNulls(size)
    }


    fun randomOrders() :ArrayList<Orden>{
      var lista = arrayListOf<Orden>()
      for(index in 0..10){
        val orden = Orden()
        val randomDaysNumbers = (Math.random() *10 ).toInt()
        var fechaRandom = DateTime()
        orden.fecha = if(randomDaysNumbers % 2 == 0) fechaRandom.plusDays(randomDaysNumbers) else fechaRandom.minusDays(randomDaysNumbers)

        orden.maiz[Queso] = (Math.random() *10 ).toInt()
        orden.maiz[Frijoles] = (Math.random() *10 ).toInt()
        orden.maiz[Revueltas] = (Math.random() *10 ).toInt()
        orden.arroz[Queso] = (Math.random() *10 ).toInt()
        orden.arroz[Frijoles] = (Math.random() *10 ).toInt()
        orden.arroz[Revueltas] = (Math.random() *10 ).toInt()
        lista.add(orden)
      }
      return lista
    }
  }

}